import { lazy } from "react";
import { Route, Routes } from "react-router-dom";

import { Principal } from "../../vistas/publicas/Principal";
import { InicioSesion } from "../../vistas/publicas/InicioSesion";
import { RegistroSesion } from "../../vistas/publicas/RegistroSesion";
import { NoEncontrado } from "../../vistas/compartidas/NoEncontrado";

//Carga perezosa componente lazy // la primera letra de la variaable debe ser en mayuscula
const LazyPrincipal = lazy(() =>
  import("../../vistas/publicas/Principal").then(() => ({ default: Principal }))); 

  const LazyInicioSesion = lazy(() =>
  import("../../vistas/publicas/InicioSesion").then(() => ({ default: InicioSesion }))); 

  const LazyRegistroSesion = lazy(() =>
  import("../../vistas/publicas/RegistroSesion").then(() => ({ default: RegistroSesion }))); 

  const LazyNoEncontrado = lazy(() =>
  import("../../vistas/compartidas/NoEncontrado").then(() => ({ default:NoEncontrado }))); 




  //RUTEO PRINCIPAL
export const RuteoCompleto = () => {
  return (
    <Routes>
      <Route path="/" element={<LazyPrincipal />} />
      <Route path="/login" element={<LazyInicioSesion />} />
      <Route path="/register" element={<LazyRegistroSesion />} />
      <Route path="*" element={<LazyNoEncontrado />} />
    </Routes>
  );
  // el asterisco es cuando no se conoce la ruta muestra un componente
};
