import ApiBack from "../utilidades/dominios/ApiBack";

 //metodo para conectar al back
 //fetch para consumir servicios
class ServicioPublico {
   
    public static async crearUsuario(miJson:any){
        const infoEnviar = {
            method:"POST",
            body:JSON.stringify(miJson),//para covertir a formato json puro
            headers:{"content-Type":"application/json; charset=UTF-8"},
            
        };

        //api de conexion
        const miUrl = ApiBack.URL+ApiBack.CREAR_USUARIO;
        //promesa 1 tehn para capturar informacion 3 catch para capturar el error
        const miRespuesta = fetch(miUrl,infoEnviar)
        .then((laRespuesta)=>laRespuesta.json())//then de capturar informacion
        .then((misDatos)=>{return misDatos;})// then 
        .catch((miError)=>{ return miError;});// then para capturar el error

        return miRespuesta;

    

    }
    
  
}

export default ServicioPublico;